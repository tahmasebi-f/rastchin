<?php

require_once 'config.php';

$css_sample = file_get_contents( 'files/ltr-style.css' );

$count_convert_file  = file_get_contents( 'files/count-convert.txt' );
$count_convert_array = explode( ',', $count_convert_file );

$count_like = file_get_contents( 'files/like.txt' );

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description"
          content="ltr to rtl css converter. Insert CSS to LTR box (left-to-right) and press convert button to get RTL CSS (right-to-left) Online. Save as rtl.css and load it after original css file."/>
    <title>LTR to RTL CSS Converter</title>
    <link rel="shortcut icon" href="assets/favicon.ico"/>
    <link rel="stylesheet" href="assets/style.min.css" media="all"/>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/styles/agate.min.css">
    <script src=" https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/highlight.min.js"></script>
</head>
<body>
<header>
    <div class="center"><h1><i class="fa fa-paragraph"></i> Rastchin, LTR to RTL CSS Converter</h1></div>
</header>

<div id="wrapper">
    <div class="ltr">
        <strong>LTR CSS</strong>
        <div id="ltr-editor"><?php echo ( isset( $_REQUEST['ltr'] ) ) ? $_REQUEST['ltr'] : $css_sample; ?></div>
    </div>

    <div class="rtl">
        <strong>RTL CSS</strong>
        <div id="rtl-editor">/* RTL CSS*/</div>
    </div>
    <p class="ltr-hint">* Please check CSS errors (<span class="red">red lines</span>) before conversion.</p>
    <form id="checkboxes">
        <input type="checkbox" id="complete" name="options[]" value="complete"> <label for="complete">All Properties</label>
        <input type="checkbox" id="reset" name="options[]" value="reset"> <label for="reset">Reset Converted Properties </label>
        <input type="checkbox" id="minify" name="options[]" value="minify"> <label for="minify">Minify </label>
    </form>

    <button id="submit" class="green-btn">CONVERT TO RTL</button>
    <p class="hint">If you find a bug or you have a suggestion, <a href="https://twitter.com/foadtahmasebi" rel="nofollow">please let me know</a>.</p>

</div>

<!--<section class="love">-->
<!--    <div class="love-inner">-->
<!--        <div class="heart"></div>-->
<!--        <div class="love-stat"><span class="loves_number"><?php echo number_format( $count_like ); ?></span></div>-->
<!--    </div>-->
<!--    <p class="hint center">Show Your Love! <span>as much as you like.<span></p>-->
<!--</section>-->


<!--<section id="stat">-->
<!--    <ul>-->
<!--        <li><span><?php echo number_format( $count_convert_array[0] ); ?></span> Conversion operations</li>-->
<!--        <li><span><?php echo number_format( $count_convert_array[1] ); ?></span> CSS Properties Have Been Checked</li>-->
<!--        <li><span><?php echo number_format( $count_convert_array[2] ); ?></span> CSS Properties Have Been Converted</li>-->
<!--    </ul>-->
<!--    <p>*Since 2/18/2018</p>-->
<!--</section>-->


<!--<section id="content">-->
<!--    <div class="gift">-->
<!--        <img src="assets/gift-card.png" width="128" height="128" title="gift">-->
<!--    </div>-->
<!--    <div class="donate">-->
<!--        <iframe src="#" width="468" height="150"-->
<!--                scrolling="no" style="border:none;display:none"></iframe>-->

<!--    </div>-->
<!--    <div class="wrap">-->
<!--        <div class="title"><h2>How To Use: <span>In regular projects</span></h2></div>-->
<!--        <div class="tips">-->
<!--            <p>-->
<!--                1. Insert "ltr css" on left box and press "Convert". <br>-->
<!--                2. Copy "rtl css" and save as <strong>"rtl.css" file</strong> in your project root.<br>-->
<!--                3. Link it after "ltr css" in your html code. like this:-->
<!--            </p>-->
<!--            <pre>-->
<!--                <code>-->
<!--  &lt;link href=&quot;ltr_style.css&quot; type=&quot;text/css&quot; rel=&quot;stylesheet&quot; media=&quot;all&quot;/&gt;-->
<!--  &lt;link href=&quot;rtl.css&quot; type=&quot;text/css&quot; rel=&quot;stylesheet&quot; media=&quot;all&quot;/&gt;-->
<!--                </code>-->
<!--            </pre>-->
<!--        </div>-->

<!--        <br><br>-->
<!--        <div class="title"><h2>How To Use: <span>In Wordpress themes</span></h2></div>-->
<!--        <div class="tips">-->
<!--            <p>-->
<!--                1. Insert "ltr css" on left box and press "Convert". <br>-->
<!--                2. Copy "rtl css" and save as <strong>"rtl.css" file</strong> in theme's directory next to "style.css".-->
<!--            </p>-->
<!--        </div>-->

<!--    </div>-->


<!--</section>-->
<!--<footer>-->
<!--    <div class="left">2018 &copy Daskhat. All Rights Reserved.</div>-->
<!--    <div class="right">-->
<!--        <nav>-->
<!--            <ul>-->
<!--                <li><a href="https://daskhat.ir">Daskhat</a></li>-->
<!--                <li>Contact me via tahmasebi.f at Gmail.</li>-->
<!--            </ul>-->
<!--        </nav>-->
<!--    </div>-->
<!--</footer>-->

<!--<div class="tabl">-->
<!--    <a href="https://t.me/dotcss" rel="nofollow" target="_blank">-->
<!--    <div class="image">-->
<!--        <img src="assets/dotcss.svg" alt="dotcss">-->
<!--    </div>-->
<!--    <div class="tabl-entry">-->

<!--        <p>dotCSS Channel</p>-->
<!--        <div>-->
<!--            <span><i class="fa fa-telegram "></i> Telegram: </span>  <span>@dotCSS </span>-->
<!--        </div>-->
<!--    </div>-->
<!--    </a>-->
<!--</div>-->

<script src="https://cdnjs.cloudflare.com/ajax/libs/ace/1.3.1/ace.js" type="text/javascript" charset="utf-8"></script>
<script>
    var ltr_editor = ace.edit('ltr-editor');
    ltr_editor.setTheme('ace/theme/chaos');
    ltr_editor.session.setMode('ace/mode/css');
    ltr_editor.session.setUseWrapMode(true);

    var rtl_editor = ace.edit('rtl-editor');
    rtl_editor.setTheme('ace/theme/chaos');
    rtl_editor.session.setMode('ace/mode/css');
    rtl_editor.session.setUseWrapMode(true);

    $(document).ready(function () {


        var AjaxURL = '<?php echo AJAX_URL; ?>';
        $('#submit').click(function () {
            $(this).html('Please wait...');
            $.ajax({
                type: 'POST',
                url: AjaxURL,
                data: {
                    ltr: ltr_editor.getValue(),
                    complete: $('#complete').is(':checked'),
                    reset: $('#reset').is(':checked'),
                    minify: $('#minify').is(':checked'),
                },
                success: function (result) {
                    $('#submit').html('CONVERT TO RTL');
                    rtl_editor.setValue(result);

                },
                complete: function () {
                    ga('create', 'UA-109541592-2', 'auto');
                    ga('send', {
                        hitType: 'pageview',
                        page: '/#rtl-editor',
                    });
                },
            });
        });

        $('pre code').each(function (i, block) {
            hljs.highlightBlock(block);
        });

        $('#checkboxes').change(function () {

            if ($('#complete').is(':checked')) {
                $('#reset').prop('checked', false);
                $('#reset').attr('disabled', 'disabled');
            } else {
                $('#reset').removeAttr('disabled');
            }
        });

        // $( '.gift' ).click( function() {
        //
        // 	$( '.donate' ).show();
        // 	$( this ).hide();
        //
        // });


        $('.heart').click(function () {
            $.ajax({
                type: 'GET',
                url: '<?php echo DOMAIN; ?>/like.php?_=' + new Date().getTime(),
                success: function (result) {
                    $('.loves_number').html(result);
                },
                error: function (request, status, error) {
                    console.log(request.responseText);
                },
                complete: function () {
                    ga('create', 'UA-109541592-2', 'auto');
                    ga('send', {
                        hitType: 'pageview',
                        page: '/#like',
                    });
                },
            });
        });

        $(window).scroll(function(){
            if($(window).scrollTop() + 100 > $(document).height() - $(window).height() ){
                $('.tabl').css('display', 'none');
            }else{
                $('.tabl').css('display', 'block');
            }
        });

    });


</script>

</body>
</html>

<?php
$count = file_get_contents( 'files/count.txt' );
$count ++;
echo "<!-- $count page view since started (3/29/2014) -->";
file_put_contents( 'files/count.txt', $count );

$count = file_get_contents( 'files/count-convert.txt' );
echo "<!-- $count convert -->";

?>
