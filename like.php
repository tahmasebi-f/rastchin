<?php

if ( ! isset( $_SERVER['HTTP_X_REQUESTED_WITH'] ) || ! ( $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest' ) ) {
	die( "please don't use directly!" ); // allow access....
}

$count_like = file_get_contents( 'files/like.txt' );

if ( $count_like ) {

	$count_like ++;
	if(is_numeric($count_like)){
    	file_put_contents( 'files/like.txt', $count_like );
    	echo number_format( $count_like );
	}
} else {
	echo '--';
}

